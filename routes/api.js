var app = require('../index');
var express = require('express');
const UserController = require( "../controller/UserController" );

// API ROUTES -------------------
// get an instance of the router for api routes
var apiRoutes = express.Router();

// route to show a random message (GET http://localhost:8000/api/)
apiRoutes.get('/', function (req, res) {
  res.json({
    message: 'Welcome to the coolest API on earth!',
    data: req.decoded
  });
});

apiRoutes.post( '/save', UserController.store );
apiRoutes.get( '/fetch', UserController.fetch );
apiRoutes.get( '/users', UserController.fetchAll );

module.exports = apiRoutes;  