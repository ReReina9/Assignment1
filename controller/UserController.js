const { User } = require('../db-mysql')

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

exports.store = (req, res) => {
    var data;
    try { 
        data = JSON.parse( req.body ) 
    } catch ( err ) {
        console.log( `/store err: ${err}` )
        data = req.body
    }
    var newUser = {
        "userID" : data.id,
        "name" : capitalizeFirstLetter(data.name),
        "surname" : capitalizeFirstLetter(data.surname),
    }
    // build + save or create
    User.create(newUser)
    .then( function( user ) {
        res.status(201).json(user);
    } )
    .catch(function(err){
        res.status(500).send(err);
    });
}

exports.fetch = ( req, res ) => {
  if ( req.query.name !== undefined & req.query.surname !== undefined ) {
    const name = capitalizeFirstLetter(req.query.name)
    const surname = capitalizeFirstLetter(req.query.surname)
    console.log(`name: ${name}, surname:${surname}`);
    User.findOne({ 
        where: { name: name, surname: surname }, 
        attributes: ['userID']})
        .then( user => {
            if ( user != null ) {
                res.status(200).json( { matricola: user.userID } ) 
            } else {
                res.status(500).json({ 
                    message: `User with name and surname = [${name}, ${surname}] doesn\'t exist.`})
            }} )
        .catch( err => res.status(400).json({ 
            err: err}) )
  } else {
      res.status(404).json({error: "Insert name and surname"})
  }
}

exports.fetchAll = ( req, res ) => {
    User.findAll()
    .then( users => res.status(200).json(users) )
    .catch( err => res.status(400).json({ error: err }) )
}