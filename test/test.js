process.env.NODE_ENV = 'test';

var express = require('express');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
let server = require('../index');
let should = chai.should();


describe('User', () => {
    it('Status 200', (done) => {
        chai.request(server)
            .get('/api/fetch?name=Davide&surname=Risso')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
    it('Status 200', (done) => {
        chai.request(server)
            .get('/api/fetch?name=Luca&surname=Reina')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
    it('Status 200', (done) => {
        chai.request(server)
            .get('/api/fetch?name=Ilaria&surname=Aquilecchia')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
    it('Status 200', (done) => {
        chai.request(server)
            .get('/api/fetch?name=Marco&surname=Trivisonno')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
    it('Status 200', (done) => {
        chai.request(server)
            .get('/api/fetch?name=Daniela&surname=Teran')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
    it('Status 200', (done) => {
        chai.request(server)
            .get('/api/fetch?name=errore&surname=errore')
            .end((err, res) => {
                res.should.not.have.status(200);
                console.log(res.matricola);
                done();
            });
    });
});




/*
const assert = require('chai').assert;
const app = require('../index').sayHello;

describe('Index', function(){
    it('index should return hello', function(){
        assert.equal(app(), 'hello');
    })
})
*/
