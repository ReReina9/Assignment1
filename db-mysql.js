const Sequelize = require('sequelize');
const UserModel = require('./model/user');
const sequelize = new Sequelize('heroku_7c5c7e38852eb0b', 'b087876e579ef1', '89b70cce', {
  host: 'us-cdbr-iron-east-01.cleardb.net',
  dialect: 'mysql',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});

const User = UserModel(sequelize, Sequelize);

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection to mysql has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the mysql database:', err);
  });

  sequelize.sync({ force: false })
  .then(() => {
    console.log(`Database & tables created!`)
  })

  module.exports = {
    User
  }
  
